(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-index-index-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/index/index.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/index/index.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header >\n  <ion-toolbar >\n    <ion-title> CUI CUI Translator </ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content class=\"ion-padding ion-margin\">\n  <ion-grid>\n    <ion-row class=\"ion-align-items-center\" >\n      <ion-col >\n          <ion-button  (click)=\"reproducir('cui_cui')\" title=\"cuiiii cuiiiii\" expand=\"block\"  color=\"secondary\">Quiero comida\n            <div (click)=\"presentAlert('Cui cuiiii', 'Cuando tu cuy quiere comida o atención', 'Muy común cuando abres el refrigerador o con el ruido de una bolsa' )\">\n              <ion-icon   class=\"ion-padding\" size=\"small\" color=\"dark\"  name=\"help-circle-outline\"></ion-icon>\n            </div>\n          </ion-button>\n        </ion-col>\n      <ion-col >\n          <ion-button  (click)=\"reproducir('cuicuicuicui')\" title=\"cui cui cui cui cui\"  expand=\"block\" color=\"secondary\">Hazme caso!\n            <div  (click)=\"presentAlert('Cui cui cui', 'Pide atención o comida', 'Te pide atención, quizas...   ¿ Hay algo de comida ?' )\">\n              <ion-icon   class=\"ion-padding\" size=\"small\" color=\"dark\"  name=\"help-circle-outline\"></ion-icon>\n            </div>\n          </ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-align-items-center\" >\n      <ion-col >\n          <ion-button  (click)=\"reproducir('happy_curiosly')\" title=\"chofff choffff\"  expand=\"block\" color=\"success\"> Estoy content@ y relajad@\n            <div (click)=\"presentAlert('Curioso y contento', 'Explorando!', 'Está emocionado y reconociendo el espacio en el que se encuentra.' )\">\n              <ion-icon   class=\"ion-padding\" size=\"small\" color=\"dark\"  name=\"help-circle-outline\"></ion-icon>\n            </div>\n          </ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-align-items-center\" >\n      <ion-col>\n          <ion-button  (click)=\"reproducir('ronroneo')\" title=\"Ronroneo\"  expand=\"block\" color=\"success\">  Me gusta! \n            <div (click)=\"presentAlert('Grrr', 'Que emoción', 'Está contento, suele emitirlo cuando lo alimentas o acaricias, este ronroneo es suave y corto.' )\">\n              <ion-icon   class=\"ion-padding\" size=\"small\" color=\"dark\"  name=\"help-circle-outline\"></ion-icon>\n            </div>\n          </ion-button>\n      </ion-col>\n      <ion-col>\n        <ion-button  (click)=\"reproducir('sex')\" title=\"Ronroneo brivante grave acompañado de balanceos\"  expand=\"block\" color=\"warning\">  Apareamiento / Territorio\n          <div  (click)=\"presentAlert('Cortejo', 'Marcando territorio', 'Ronroneo fuerte y brivante, suele estar acompañado de un balanceo con el cuerpo, si no hay otro cuyo cerca es que está molesto o asustado.' )\">\n            <ion-icon   class=\"ion-padding\" size=\"small\" color=\"dark\"  name=\"help-circle-outline\"></ion-icon>\n          </div>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-align-items-end\" >\n      <ion-col>\n        <ion-button  (click)=\"reproducir('quejido')\" title=\"quejido\"  expand=\"block\" color=\"warning\">  Déjame en paz! \n          <div  (click)=\"presentAlert('Molestia o disgusto', 'Puede enfadarse', 'Molestia al ser acariciado o por algun otro cuyo, tambien cuando es perseguido, se debe intentar terminar con esta situación lo antes posbile para que no se enfade.' )\"> \n            <ion-icon   class=\"ion-padding\" size=\"small\" color=\"dark\"  name=\"help-circle-outline\"></ion-icon>\n          </div>\n        </ion-button>\n      </ion-col>\n      <ion-col>\n        <ion-button  (click)=\"reproducir('angry')\" title=\"castañeo de dientes\"  expand=\"block\" color=\"danger\">  Estoy enfadad@!! \n          <div  (click)=\"presentAlert('Agresividad', 'Cuidado que muerdo!', 'Se debe dejar tranquilo al cuyo, puede atacar, puede ir acompañado de alzamientos de cabeza o bostezos enseñando los dientes de forma agresiva, podría atacar. Si es entre dos cuyos o más se deben separar o podrían lastimarse mutuamente.' )\">\n            <ion-icon   class=\"ion-padding\" size=\"small\" color=\"dark\"  name=\"help-circle-outline\"></ion-icon>\n          </div>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-align-items-end\" >\n      <ion-col>\n        <ion-button  (click)=\"reproducir('pastoreo')\" title=\"pastoreo\"  expand=\"block\" color=\"success\">  Masticando \n          <div  (click)=\"presentAlert('Mmmm', 'Relajante escucharlos pastando heno', 'Siempre deben tener heno a su disposición, mientras má verde más les gustará, no olvides llenar su henera antes de trabajar o salir durante varias horas.' )\">\n            <ion-icon   class=\"ion-padding\" size=\"small\" color=\"dark\"  name=\"help-circle-outline\"></ion-icon>\n          </div>\n        </ion-button>\n      </ion-col>\n      <ion-col>\n        <ion-button  (click)=\"reproducir('canto')\" title=\"Piar de pájaro\"  expand=\"block\" color=\"secondary\">  ¿?  Canto ¿? \n          <div  (click)=\"presentAlert('¿Eso es un pájaro?', '¿Fuiste tú cuyito?', 'Poco común y raro de escuchar, no se sabe muy bien que necesita aunque se creee que puede estar estresado o la llamada de un bebé a su madre para ser alimentado.' )\">\n            <ion-icon   class=\"ion-padding\" size=\"small\" color=\"dark\"  name=\"help-circle-outline\"></ion-icon>\n          </div>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n      <ion-row class=\"ion-align-items-end\" >\n      <ion-col>\n        <ion-button  (click)=\"reproducir('chillidos')\" title=\"chillido agudo de dolor\"  expand=\"block\" color=\"danger\">  Ayyy!!  \n          <div (click)=\"presentAlert('¿Qué te pasa?', 'Revisa a tu cuy, ¡algo le ocurre!', 'Chillido agudo en respuesta a un dolor o peligro inmediato.  ' )\">\n            <ion-icon  class=\"ion-padding\"  size=\"small\" color=\"dark\" name=\"help-circle-outline\"></ion-icon>\n          </div>\n        </ion-button>   \n      </ion-col>\n      <ion-col>\n        <ion-button  (click)=\"reproducir('tos')\" title=\"tos o atragantamiento\"  expand=\"block\" color=\"warning\">  Achís  \n          <div (click)=\"presentAlert('Achís', 'Estornudos o atragantamientos', 'Común cuando beben o comen muy rápido nada de lo que preocuparse. Si lo hacen frecuentemente cuida que no respiren polvo del sustrato (serrín o pellets de madera) ya que irrita sus pulmones, si continua debe ser llevado a un veterinarios para comprobar infecciones respiratorias u obstrucciones de garganta.' )\">\n            <ion-icon   class=\"ion-padding\" size=\"small\" color=\"dark\"  name=\"help-circle-outline\"></ion-icon>\n          </div>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n\n  </ion-grid>\n\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/index/index-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/index/index-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: IndexPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IndexPageRoutingModule", function() { return IndexPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _index_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./index.page */ "./src/app/pages/index/index.page.ts");




const routes = [
    {
        path: '',
        component: _index_page__WEBPACK_IMPORTED_MODULE_3__["IndexPage"]
    }
];
let IndexPageRoutingModule = class IndexPageRoutingModule {
};
IndexPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], IndexPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/index/index.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/index/index.module.ts ***!
  \*********************************************/
/*! exports provided: IndexPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IndexPageModule", function() { return IndexPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _index_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./index-routing.module */ "./src/app/pages/index/index-routing.module.ts");
/* harmony import */ var _index_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./index.page */ "./src/app/pages/index/index.page.ts");







let IndexPageModule = class IndexPageModule {
};
IndexPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _index_routing_module__WEBPACK_IMPORTED_MODULE_5__["IndexPageRoutingModule"]
        ],
        declarations: [_index_page__WEBPACK_IMPORTED_MODULE_6__["IndexPage"]]
    })
], IndexPageModule);



/***/ }),

/***/ "./src/app/pages/index/index.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/index/index.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2luZGV4L2luZGV4LnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/index/index.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/index/index.page.ts ***!
  \*******************************************/
/*! exports provided: IndexPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IndexPage", function() { return IndexPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_admob_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/admob.service */ "./src/app/services/admob.service.ts");



//IMPORTAMOS NUESTRO SERVICIO

let IndexPage = class IndexPage {
    constructor(alertController, 
    //AÑADIMOS AL CONSTRUCTOR.
    admobService) {
        this.alertController = alertController;
        this.admobService = admobService;
    }
    ngOnInit() {
        //AL CARGAR LA PAGINA MOSTRAMOS BANNER
        this.admobService.MostrarBanner();
    }
    //FUNCION PARA LLAMAR AL INTERSTITIAL
    MostrarInterstitial() {
        this.admobService.MostrarInterstitial();
    }
    //FUNCION PARA LLAMAR AL VIDEOREWARD
    MostrarReward() {
        this.admobService.MostrarRewardVideo();
    }
    //reproducir sonido pasandole nombre del fichero de audio
    reproducir(name) {
        const audio = new Audio('../../../assets/audio/' + name + '.mp3');
        //alert('../../../assets/audio/'+nombre+'.mp3');
        audio.play();
    }
    //muestra alerta pasandole, titulo, subtirulo y cuerpo del mensaje.
    presentAlert(title, subtitle, body) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: title,
                subHeader: subtitle,
                message: body,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
};
IndexPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _services_admob_service__WEBPACK_IMPORTED_MODULE_3__["AdmobService"] }
];
IndexPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-index',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./index.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/index/index.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./index.page.scss */ "./src/app/pages/index/index.page.scss")).default]
    })
], IndexPage);



/***/ })

}]);
//# sourceMappingURL=pages-index-index-module-es2015.js.map