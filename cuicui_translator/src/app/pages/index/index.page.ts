import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
//IMPORTAMOS NUESTRO SERVICIO
import { AdmobService } from '../../services/admob.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss'],
  
})
export class IndexPage implements OnInit {

  constructor(
    public alertController: AlertController,
    //AÑADIMOS AL CONSTRUCTOR.
    private admobService: AdmobService
    ) { 
    
  }

  ngOnInit() {
    //AL CARGAR LA PAGINA MOSTRAMOS BANNER
    this.admobService.MostrarBanner();
  }

  //FUNCION PARA LLAMAR AL INTERSTITIAL
  MostrarInterstitial(){
    this.admobService.MostrarInterstitial();
  }
  //FUNCION PARA LLAMAR AL VIDEOREWARD
  MostrarReward(){
    this.admobService.MostrarRewardVideo();
  }
  //reproducir sonido pasandole nombre del fichero de audio
  reproducir(name) {
    const audio = new Audio('../../../assets/audio/'+name+'.mp3');
    //alert('../../../assets/audio/'+nombre+'.mp3');
    audio.play();
  }
  //muestra alerta pasandole, titulo, subtirulo y cuerpo del mensaje.
  async presentAlert(title, subtitle, body) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: title,
      subHeader: subtitle,
      message: body,
      buttons: ['OK']
    });

    await alert.present();
  }

}
